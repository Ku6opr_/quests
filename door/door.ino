byte B[10];
byte B_[10];

byte last[4] = {0, 0, 0, 0};
byte code[] = {1, 5, 3, 9};

byte opened = 0;

void setup() {
  Serial.begin(9600);
  
  pinMode(5, INPUT); 
  digitalWrite(5, HIGH);
  pinMode(6, INPUT); 
  digitalWrite(6, HIGH);
  pinMode(7, INPUT); 
  digitalWrite(7, HIGH);
  pinMode(8, INPUT); 
  digitalWrite(8, HIGH);
  
  pinMode(2, OUTPUT); 
  digitalWrite(2, HIGH);
  pinMode(3, OUTPUT); 
  digitalWrite(3, HIGH);
  pinMode(4, OUTPUT); 
  digitalWrite(4, HIGH);
  
  pinMode(12, OUTPUT); 
  digitalWrite(12, HIGH);
}

void loop() {
  digitalWrite(2, LOW);
  digitalWrite(3, HIGH);
  digitalWrite(4, HIGH);
  
  B_[1] = !digitalRead(8);
  B_[4] = !digitalRead(7);
  B_[7] = !digitalRead(6);
    
  delay(10);
  
  digitalWrite(2, HIGH);
  digitalWrite(3, LOW);  
  digitalWrite(4, HIGH);
  
  B_[2] = !digitalRead(8);
  B_[5] = !digitalRead(7);
  B_[8] = !digitalRead(6);
  B_[0] = !digitalRead(5);
  
  delay(10);
  
  digitalWrite(2, HIGH);
  digitalWrite(3, HIGH);
  digitalWrite(4, LOW);
  
  B_[3] = !digitalRead(8);
  B_[6] = !digitalRead(7);
  B_[9] = !digitalRead(6);
  
  delay(10);
  
  for (int i = 0; i < 10; i++)
  {
    if(B[i] != B_[i])
    {
      B[i] = B_[i];
      
      if (last[3] != i)
      {
        last[0] = last[1];
        last[1] = last[2];
        last[2] = last[3];
        last[3] = i;
        
        Serial.print(last[0]);
        Serial.print(last[1]);
        Serial.print(last[2]);
        Serial.print(last[3]);
        Serial.println("");
        
        if (!opened && last[0] == code[0] && last[1] == code[1] && last[2] == code[2] && last[3] == code[3])
        {
           Serial.println("OPEN");
           
           digitalWrite(12, LOW);
           
           opened = 1;
        } 
      }
    }
  }
  
  if (opened && B[0])
  {
    Serial.println("CLOSE");
    
    digitalWrite(12, HIGH);
           
    opened = 0;
  }
}
