int digits[][8] = {{10, 5, 19, 17, 16, 8}, {5, 19}, {10, 5, 4, 16, 17}, {10, 5, 4, 19, 17}, {8, 4, 5, 19}, {10, 8, 4, 17, 19}, {10, 8, 16, 4, 17, 19}, {10, 5, 19}, {10, 5, 19, 17, 16, 8, 4}, {10, 5, 19, 17, 8, 4}};

int positions[] = {11, 6, 7, 3};

volatile long startTime = 0;
volatile long countdown = 3600;
volatile long time = 0;

volatile byte play = 0;

volatile int playButton = 0;
volatile int resetButton = 0;
volatile int incrementButton = 0;

int buttonsTheshold = 50;

void displayTime()
{
  unsigned long minutes = time / 60;
  unsigned long seconds = time - minutes * 60;
  
  byte minutes1 = minutes / 10;
  byte minutes2 = minutes - minutes1 * 10;
  
  byte seconds1 = seconds / 10;
  byte seconds2 = seconds - seconds1 * 10;
  
  displayDigit(0, minutes1);
  delayMicroseconds(100);
  displayDigit(1, minutes2);
  delayMicroseconds(100);
  displayDigit(2, seconds1);
  delayMicroseconds(100);
  displayDigit(3, seconds2);
  delayMicroseconds(100);
  
  clearDigits();
  
  if (play)
  {
    if (millis() / 500 % 2 == 0)
    {
      displayDelimeter(1);
      
      if (time <= 20)
      {
        digitalWrite(9, HIGH);
        delayMicroseconds(100);
        digitalWrite(9, LOW);
      }
    }
  }
  else  
  {
    displayDelimeter(1);
  }
  
  delayMicroseconds(100);
  
  clearDigits();
}

void setup() {
  pinMode(10, OUTPUT);
  digitalWrite(10, LOW);
  pinMode(5, OUTPUT);
  digitalWrite(5, LOW);
  pinMode(19, OUTPUT);
  digitalWrite(19, LOW);
  pinMode(17, OUTPUT);
  digitalWrite(17, LOW);
  pinMode(16, OUTPUT);
  digitalWrite(16, LOW);
  pinMode(8, OUTPUT);
  digitalWrite(8, LOW);
  pinMode(4, OUTPUT);
  digitalWrite(4, LOW);
  pinMode(18, OUTPUT);
  digitalWrite(18, LOW);
  
  pinMode(11, OUTPUT);
  digitalWrite(11, LOW);
  pinMode(6, OUTPUT);
  digitalWrite(6, LOW);
  pinMode(7, OUTPUT);
  digitalWrite(7, LOW);
  pinMode(3, OUTPUT);
  digitalWrite(3, LOW);
  
  pinMode(A1, INPUT);
  digitalWrite(A1, HIGH);
  pinMode(A0, INPUT);
  digitalWrite(A0, HIGH);
  //pinMode(A7, INPUT);
  //digitalWrite(A7, HIGH);
  
  pinMode(9, OUTPUT);
  digitalWrite(9, LOW);
  
  startTime = millis() / 1000;
  time = 0;
  
  delay(100);
  
  Serial.begin(9600);
}

void loop() {
  
  if (play)
  {
    time = countdown - (millis() / 1000 - startTime);
    
    if (time == 0)
    {
      play = 0;
    }
  }
  
  updateButtons();
  
  displayTime();
}

void clearDigits()
{
  digitalWrite(positions[0], LOW);
  digitalWrite(positions[1], LOW);
  digitalWrite(positions[2], LOW);
  digitalWrite(positions[3], LOW);
  
  digitalWrite(10, LOW);
  digitalWrite(5, LOW);
  digitalWrite(19, LOW);
  digitalWrite(17, LOW);
  digitalWrite(16, LOW);
  digitalWrite(8, LOW);
  digitalWrite(4, LOW);
  digitalWrite(18, LOW);
}

void displayDigit(int position, int number)
{
  clearDigits();
  
  for (int i = 0; i < 8; i++)
  {
    if (digits[number][i] != 0)
    {
      digitalWrite(digits[number][i], HIGH);
    }
  }
  
  digitalWrite(positions[position], HIGH);
}

void displayDelimeter(int position)
{
  clearDigits();
  
  digitalWrite(positions[position], HIGH);
  digitalWrite(18, HIGH);
}

void updateButtons()
{
  
  if (!digitalRead(A1))
  {
    playButton++;
    
    if (playButton == buttonsTheshold)
    {
      byte play_ = (play == 0) ? 1 : 0;
      
      if (play_ == 1)
      {
        if (time == 0)
        {
          startTime = millis() / 1000;
        }
        else        
        {
          startTime = millis() / 1000 - (countdown - time);
        }
      }
      else      
      {
      
      }
      
      play = play_;
    }
  }
  else  
  {    
    playButton = 0;
  }
  
  if (!digitalRead(A0))
  {
    resetButton++;
    
    if (resetButton == buttonsTheshold)
    {
      startTime = millis() / 1000;
      countdown = 3600;
      time = 0;
      play = 0;
    }
  }
  else  
  {
    resetButton = 0;
  }
  
  if (analogRead(A7) < 50)
  {
    incrementButton++;
    
    if (incrementButton == buttonsTheshold)
    {
      countdown+=60;
      time+=60;
    }
    else if (incrementButton > buttonsTheshold * 5 && incrementButton % (buttonsTheshold * 2) == 0)
    {
      countdown+=60;
      time+=60;
    }
    else if (incrementButton > buttonsTheshold * 25 && incrementButton % buttonsTheshold == 0)
    {
      countdown+=60;
      time+=60;
    }

    if (time > 99 * 60)
    {
      time = countdown = 99 * 60;
    }
  }
  else  
  {
    incrementButton = 0;
  }
}
